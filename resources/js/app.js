// Importing Lodash and jQuery
window._ = require('lodash');

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

} catch (e) {}

// Import Axios and setting header

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

// CSRF token

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

// Import Vue.js and use the app main component

window.Vue = require('vue');

Vue.component('App', require('./pages/App.vue').default);
Vue.component('Login', require('./pages/Login').default);
Vue.component('Main', require('./pages/Main').default);

// Import libraries for Vue use

import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'
import router from './routes.js'

Vue.use(Vuetify);


const app = new Vue({
    el: '#app',
    router
});
