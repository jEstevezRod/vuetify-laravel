import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);


const router = new VueRouter({
    routes: [

        {
            path: '/',
            name: 'Login',
            component: Vue.component('Login', require('./pages/Login').default)
        },
        {
            path: '/app',
            name: 'App',
            component: Vue.component('App', require('./pages/App').default)

        },
        {
            path: '/main',
            name: 'Main',
            component: Vue.component('Main', require('./pages/Main').default)

        },


    ]
});

export default router