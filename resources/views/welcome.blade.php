<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Jesús Panel</title>
    <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico' />
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600|Varela+Round" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        <App></App>
    </div>
    
</body>
<script src="{{ asset('js/app.js') }}"></script>

</html>